# Install Docker CE

## Установка Docker на голую OS Ubuntu 16.04

1. Для начала требуется обновить индексы репозитариев

   ```
   $ sudo apt-get update
   ```

2. Установить пакеты из ниже представленного списка:

   ```
   $ sudo apt-get install \
       apt-transport-https \
       ca-certificates \
       curl \
       gnupg-agent \
       software-properties-common
   ```

3. Добавляем офицальный  GPG ключ:

   ```
   $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   ```

   Проверяем наш добавленный ключь финджер принт должен показать следующее значение `9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`, 

   ```
   $ sudo apt-key fingerprint 0EBFCD88
       
   pub   rsa4096 2017-02-22 [SCEA]
         9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
   uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
   sub   rsa4096 2017-02-22 [S]
   ```

4. Добавлеяем репозитарий docker 

   ```
   $ sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"
   ```

   

#### 

5. Опять обновляем индыксы репозитариев

```
$ sudo apt-get update
```

6. приступаем к установке последней стабильной версии Docker CE

```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

7. Для проверки работы контейнера запускаем образ контейнера  `hello-world` .

```
$ sudo docker run hello-world
```

в ответ он должен вернуть следующее

```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Установка Docker-compose 

скачиваем скрипт в директорию /usr/local/bin/

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

скорей всего потребуется запустить curl с ключом 

и делаем его исполняемым 

```
sudo chmod +x /usr/local/bin/docker-compose
```